# ShaKikoo

Algorithme de hachage personnalisé créé par **Atelier 801**, réimplementé par **Tritium Dev** dans plusieurs langages

## Avancement

- [Javascript](https://gitlab.com/tritium_dev/lib-ShaKikoo/tree/master/JavaScript) : terminé (implémentation par Atelier 801)
- [Python](https://gitlab.com/tritium_dev/lib-ShaKikoo/tree/master/Python) : terminé (implémentation par Nathan, réécrite par Hugo pour + de lisibilité)
- [PHP](https://gitlab.com/tritium_dev/lib-ShaKikoo/tree/master/PHP) : terminé (implémentation par Nathan)
- [C#](https://gitlab.com/tritium_dev/lib-ShaKikoo/tree/master/C%23) : terminé (implémentation lisible par Hugo)

## Utilisation

#### Javascript

```javascript
alert("A faire")
```

#### Python
Nécessite d'avoir shakikoo.py dans le même dossier (ou dans un autre dossier du Python Path)
```python
import shakikoo
mdpEnClair = "MotDePasseEnClair"
mdpHashe = shakikoo.hash(mdpEnClair)
```

#### PHP

```php
echo("A faire");
```

#### C# ####
Nécessite d'inclure l'assembly ShaKikoo (dll binaire disponible directement dans le répo) dans le projet
```csharp
using ShaKikoo;
string mdpEnClair = "MotDePasseEnClair"
string mdpHashe = ShaKikoo_Hasher.Hash(mdpEnClair)
```
