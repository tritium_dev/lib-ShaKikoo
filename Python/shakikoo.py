# -*- coding: utf-8 -*-

# Réécriture plus lisible par Hugo de l'implémentation de ShaKikoo en Python faite par Nathaan

import binascii
import hashlib
import base64

def hash(toHash):
    # Constantes
    CRYPT_VALUES = [-9, 25, -92, -37, -117, 18, 112, -95, -5, -108, 40, -83, -107, 73, -92, -102, 46, -52, 49, -118, -79, -56, -72, 63, -69, -98, -118, -22, 46, -16, -22, -111]
    HEX_CHARS = "0123456789abcdef"

    # Hash en SHA256
    toHash_sha256 = hashlib.sha256(toHash).hexdigest()

    ShaKikooBytes = []
    # Etape 1 : Mettre dans ShaKikooBytes tous les caractères de toHash_sha256, transformés en bytes signés
    for char in toHash_sha256:
        ShaKikooBytes.append(ord(char))

    # Etape 2 : Mettre dans ShaKikooBytes toutes les cryptValues auxquelles sont ajoutées leur indice
    for indice in range(0, len(CRYPT_VALUES)):
        ShaKikooBytes.append(int(CRYPT_VALUES[indice] + indice))
    
    # Etape 3 : On transforme l'array ShaKikooBytes en une chaîne hexadécimale, en récupérant les indices à utiliser pour hexChars
    ShaKikooHex = ""
    for byte in ShaKikooBytes:
        # Premier indice : le byte, décalé de 4 bits pour obtenir une valeur hexadécimale (0-15). (4 bits car 2^4 = 16)
        firstId = (byte >> 4) & 15
        # Deuxième indice : le reste du byte
        secondId = byte & 15
        # On rajoute les caractères hexadécimaux
        ShaKikooHex = ShaKikooHex + HEX_CHARS[firstId] + HEX_CHARS[secondId]

    # Etape 4 : Transformations diverses
    ShaKikooHex_bin = binascii.unhexlify(ShaKikooHex) # Hexadécimal => Binaire
    ShaKikooHex_sha256_bin = hashlib.sha256(ShaKikooHex_bin).digest() # Binaire => SHA256 Binaire
    ShaKikooHex_sha256_b64 = base64.b64encode(ShaKikooHex_sha256_bin) # SHA256 Binaire => SHA256 Base64

    return ShaKikooHex_sha256_b64