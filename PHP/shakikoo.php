<?php

# Implémentation de ShaKikoo en PHP (par Nathaan)

function toHex($h, $f=true, $b=null) {
	$e = "0123456789abcdef";
	$d = "";
	foreach (range(0,3) as $c) {
		$d = $d . $e[($h >> ((3 - $c) * 8 + 4)) & 15] . $e[($h >> ((3 - $c) * 8)) & 15];
	}
	if ($b !== null) {
		return substr($d, strlen($d) - $b);
	} else {
	    return $d;
	}
	
	return;
}

function crypte($a) {
    $e = array(-9, 25, -92, -37, -117, 18, 112, -95, -5, -108, 40, -83, -107, 73, -92, -102, 46, -52, 49, -118, -79, -56, -72, 63, -69, -98, -118, -22, 46, -16, -22, -111);
    $j = hash("sha256", $a);
    $h = array();
    foreach (range(0, strlen($j)-1) as $b) {
        array_push($h, ord($j[$b]));
    }
    foreach (range(0, count($e)-1) as $b) {
        array_push($h, $e[$b] + $b);
    }
    
    $d = "";
    
    foreach (range(0, count($h)-1) as $b) {
        $d = $d . toHex($h[$b], true, 2);
    }
    
    foreach (range(0, count($e)-1) as $b) {
        array_push($h, $e[$b] + $b);
    }
    
    $c = pack('H*', $d);
    $f = pack('H*', hash("sha256", $c));
    return base64_encode($f);
}

echo crypte("test");