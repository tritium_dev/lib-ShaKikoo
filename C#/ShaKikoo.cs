﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Globalization;

// Implémentation lisible de ShaKikoo en C# faite par Hugo

namespace ShaKikoo
{
    static public class ShaKikoo_Hasher
    {
        static private readonly sbyte[] cryptValues = new sbyte[] { -9, 25, -92, -37, -117, 18, 112, -95, -5, -108, 40, -83, -107, 73, -92, -102, 46, -52, 49, -118, -79, -56, -72, 63, -69, -98, -118, -22, 46, -16, -22, -111 };
        static private readonly string hexChars = "0123456789abcdef";

        static public string Hash(string toHash)
        {           
            // Hash en SHA256
            var toHash_sha256 = sha256_hash(toHash);

            var ShaKikooBytes = new List<sbyte>();
            // Etape 1 : Mettre dans ShaKikooBytes tous les caractères de toHash_sha256, transformés en bytes signés
            foreach (char i in toHash_sha256)
                ShaKikooBytes.Add(Convert.ToSByte(i));

            // Etape 2 : Mettre dans ShaKikooBytes toutes les cryptValues auxquelles sont ajoutées leur indice
            for(int indice = 0; indice < cryptValues.Length; indice++)
            {
                ShaKikooBytes.Add((sbyte)(cryptValues[indice] + indice));
            }

            // Etape 3 : On transforme l'array ShaKikooBytes en une chaîne hexadécimale, en récupérant les indices à utiliser pour hexChars
            var ShaKikooHex = "";
            foreach (sbyte i in ShaKikooBytes)
            {
                // Premier indice : le byte, décalé de 4 bits pour obtenir une valeur hexadécimale (0-15). (4 bits car 2^4 = 16)
                var firstId = (i >> 4) & 15;
                // Deuxième indice : le reste du byte
                var secondId = i & 15;
                // On rajoute les caractères hexadécimaux
                ShaKikooHex = ShaKikooHex + hexChars[firstId] + hexChars[secondId];
            }

            // Etape 4 : Transformations diverses
            var ShaKikooHex_bin = Hex2Binary(ShaKikooHex); // Hexadécimal => Binaire
            var ShaKikooHex_sha256_bin = sha256_hash(ShaKikooHex_bin); // Binaire => SHA256 Binaire
            var ShaKikooHex_sha256_b64 = Convert.ToBase64String(ShaKikooHex_sha256_bin); // SHA256 Binaire => SHA256 Base64

            // On a fini, hourra !
            return ShaKikooHex_sha256_b64;
        }

        #region Private Methods

        private static String sha256_hash(String value)
        {
            using (SHA256 hasher = SHA256Managed.Create())
            {
                return String.Join("", hasher
                  .ComputeHash(Encoding.UTF8.GetBytes(value))
                  .Select(item => item.ToString("x2")));
            }
        }

        private static byte[] sha256_hash(byte[] value)
        {
            using (SHA256 hasher = SHA256Managed.Create()){
                return hasher.ComputeHash(value);
            }
        }

        private static byte[] Hex2Binary(string hex)
        {
            var chars = hex.ToCharArray();
            var bytes = new List<byte>();
            for (int index = 0; index < chars.Length; index += 2)
            {
                var chunk = new string(chars, index, 2);
                bytes.Add(byte.Parse(chunk, NumberStyles.AllowHexSpecifier));
            }
            return bytes.ToArray();
        }
        
        #endregion Private Methods
    }
}
